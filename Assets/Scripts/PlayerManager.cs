﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
	Animator animator;
	public AudioClip soundHit;
	public AudioClip soundWalk;
	AudioSource audioSource;
    int playerState;

    public int PlayerState
    {
        get
        {
            return playerState;
        }

        set
        {
            playerState = value;
        }
    }

    // Use this for initialization
    void Start () 
	{
		animator = GetComponent<Animator> ();
		audioSource = GetComponent<AudioSource> ();
        PlayerState = 0;
    }
	
	// Update is called once per frame
	void Update () 
	{
        // handle keyboard input
        handleKeyboardInput();

        // handle touch events from the GestureHandler
        handleTouchInput();
    }

    /// <summary>
    /// Change animation and sound effects played, based on
    /// the PlayerState which is changed by the 
    /// GestureHandler script.
    /// </summary>
    void handleTouchInput()
    {
        // Setting to Idle State for touch input
        // this needs to be before all other handlers
        // so that we don't immediately cancel the audio loop
        if (PlayerState == 0)
        {
            //change the state on the animator parameter
            animator.SetInteger("state", 0);
            audioSource.loop = false;
        }

        // Change to hit state
        if (PlayerState == 2)
        {
            //change the state on the animator parameter
            animator.SetInteger("state", 2);
            audioSource.PlayOneShot(soundHit, 1.0f);
            // Because a hit event is a once-off,
            // we will set PlayerState back to idle
            // once we have handled it.
            PlayerState = 0;
        }
        if (PlayerState == 1)
        {
            //change the state on the animator parameter
            animator.SetInteger("state", 1);
            audioSource.loop = true;
            audioSource.clip = soundWalk;

            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }
    }

    /// <summary>
    /// Handle keyboard input events.
    /// </summary>
    void handleKeyboardInput()
    {

        // change state based on keyboard state
        // Walk state
        if (Input.GetKeyDown(KeyCode.W))
        {
            //change the state on the animator parameter
            animator.SetInteger("state", 1);
            audioSource.loop = true;
            audioSource.clip = soundWalk;
            audioSource.Play();
        }
        // Walk State to Idle State
        if (Input.GetKeyUp(KeyCode.W))
        {
            //change the state on the animator parameter
            animator.SetInteger("state", 0);
            audioSource.loop = false;
        }

        // change state based on keyboard state
        // Attack State
        if (Input.GetKeyDown(KeyCode.F))
        {
            //change the state on the animator parameter
            animator.SetInteger("state", 2);
            //audioSource.PlayOneShot (soundHit, 1.0f);
            audioSource.loop = true;
            audioSource.clip = soundHit;
            audioSource.Play();
        }

        // Attack State to Idle State
        if (Input.GetKeyUp(KeyCode.F))
        {
            //change the state on the animator parameter
            animator.SetInteger("state", 0);
            audioSource.loop = false;
        }
    }
}
