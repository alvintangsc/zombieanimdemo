﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TouchScript.Gestures;
using System;

public class GestureHandler : MonoBehaviour
{
    // reference to the player we want this gesture handler to affect
    // remember to drag-drop the player object to this script in the Unity inspector
    public GameObject player;

    PlayerManager playerManagerScript;

    // Use this for initialization
    void Start ()
    {
        playerManagerScript = player.GetComponent<PlayerManager>();	
	}
	
	// Update is called once per frame
	void Update ()
    {
		// nothing to do here
	}

    private void OnEnable()
    {
        GetComponent<FlickGesture>().Flicked += flickHandler;
        GetComponent<TapGesture>().Tapped += tapHandler;
        GetComponent<LongPressGesture>().LongPressed += longPressedHandler;
    }

    private void longPressedHandler(object sender, EventArgs e)
    {
        playerManagerScript.PlayerState = 0;
    }

    private void tapHandler(object sender, EventArgs e)
    {
        playerManagerScript.PlayerState = 1;
    }

    private void flickHandler(object sender, EventArgs e)
    {
        playerManagerScript.PlayerState = 2;
    }

    private void OnDisable()
    {
        GetComponent<FlickGesture>().Flicked -= flickHandler;
        GetComponent<TapGesture>().Tapped -= tapHandler;
        GetComponent<LongPressGesture>().LongPressed -= longPressedHandler;
    }
}
